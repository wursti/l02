.icon:briefcase[] Cisco Packet Tracer
****
Der Cisco Packet Tracer, kurz CPT, ist eine Netzwerk-Simulations-Software.
Das Werkzeug von Cisco eignet sich zum Beispiel für die Ausbildung zum Fachinformatikern, um den Umgang mit Netzwerken zu erlernen und insbesondere das Verhalten bei Problemen.

Der CPT bietet die Möglichkeit verschiedene Szenarien nachzustellen.
Dabei ermöglicht er das Sie der Datenübertragung "zusehen" können.
Damit ist der CPT das ideale Werkzeug um ein tieferes Verständnis der Abläufe in einem vernetzten IT-System zu bekommen.

NOTE: Der Cisco Packet Tracer ist kostenlos erhältlich, bedarf allerdings einer Registrierung bei der Cisco Networking Academy. +
https://www.netacad.com/courses/packet-tracer


.Cisco Packet Tracer: Simulation mit 6 Netzwerken
image:cpt_6xNetze.png[]

//Download: link:{attachmentsdir}/cpt_6xNetze.pkt[cpt_6xNetze]
****