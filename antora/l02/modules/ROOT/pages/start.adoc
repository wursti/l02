= Das Lernfeld 2

== {l02}

// Thema vorstellen und wichtige Punkte #markieren#.
// Satz für Satz erläutern

.Der Rahmenlehrplan sagt dazu...
****
Die Teilnehmenden verfügen über die Kompetenz, die Ausstattung eines Arbeitsplatzes nach Kundenwunsch zu dimensionieren, anzubieten, zu beschaffen und den Arbeitsplatz an die Kunden zu übergeben. +
Die Teilnehmenden nehmen den Kundenwunsch für die Ausstattung eines Arbeitsplatzes von internen und externen Kunden entgegen und ermitteln die sich daraus ergebenden Anforderungen an Soft- und Hardware. +
Aus den dokumentierten Anforderungen leiten sie Auswahlkriterien für die Beschaffung ab.
****

////
Sie können noch so viele Tools nehmen.
Sie müssen immernoch wissen was Sie tun. Das ist Kompetenz.
////