.icon:briefcase[] ipconfig
****
Zeigt die aktuellen TCP/IP-Netzwerk Konfigurationswerte an.

IMPORTANT: Die Benennung und der Funktionsumfang des Befehls variiert je nach Implementierung.
Es gilt die Regel *RTFM* footnote:[*Read The Fucking Manual*: Oder etwas freundlicher.
Für nähere Informationen konsultieren Sie bitte die Bedienungsanleitung.].

.Ermitteln der "eigenen" MAC-Adresse
[width="100%",options="header"]
|====================
|Betriebssystem  |Kommando(s)
| Linux |   ``ifconfig -a +
            ip link show <Schnittstelle>``
| FreeBSD/macOS | ``ifconfig -a``
| Windows | ``ipconfig /all``
|====================

NOTE: Unter der PowerShell werden die Befehle `Get-NetAdapter` und `Get-NetIPConfiguration` verwendet.

****
