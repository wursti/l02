.icon:briefcase[] ping
****
Ping zählt zu der Gruppe der Diagnose-Programmen. Ping wird meist dazu verwendet um die Erreichbarkeit eines `Hosts` zu testen.
Weiterhin gibt der Ping aufschluss über die `Latenz` einer Verbindung.
Dieser Befehl steht auf allen gängigen Betriebssystemen zur Verfügung.

link:https://docs.microsoft.com/de-de/windows-server/administration/windows-commands/ping[Microsoft Docs]

Ping auf die Website des BND

[source]
----
christian@MacBook ~ % ping www.bnd.de -c 4

PING www.bnd.de (149.232.252.3): 56 data bytes
64 bytes from 149.232.252.3: icmp_seq=0 ttl=58 time=20.393 ms
64 bytes from 149.232.252.3: icmp_seq=1 ttl=58 time=20.082 ms
64 bytes from 149.232.252.3: icmp_seq=2 ttl=58 time=19.777 ms
64 bytes from 149.232.252.3: icmp_seq=3 ttl=58 time=19.947 ms

--- www.bnd.de ping statistics ---
4 packets transmitted, 4 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 19.777/20.050/20.393/0.226 ms
----

NOTE: Unter der PowerShell wird der Befehl `Test-NetConnection <IP-Adresse>` verwendet.
****
